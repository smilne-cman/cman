#include <stdio.h>
#include <stdlib.h>
#include <libtest/libtest.h>
#include <libfiles/libfiles.h>
#include <libcollection/list.h>
#include <libcollection/types.h>
#include "utility.h"

/* Types **********************************************************************/

/* Prototypes *****************************************************************/
void test_join();
void test_get_default();
void test_is_lib();
void test_prefix();
static FileInfo *make_file(const char *name, const char *path, const char *ext);

/* Global Variables ***********************************************************/

int main(int argc, char *argv[]) {
  test_init("Utility", argc, argv);

  test("Join - Should join filenames with delimiter", test_join);
  test("Get Default - Should return given value or default if empty", test_get_default);
  test("Is Lib - Should check if string starts with 'lib'", test_is_lib);
  test("Prefix - Should prefix the given string", test_prefix);

  return test_complete();
}

void test_join() {
  List *files = list_new(ListAny);

  list_add(files, make_file("file1", "dir", "txt"));
  list_add(files, make_file("file2", "dir", "ini"));
  list_add(files, make_file("file3", "dir", "txt"));

  char *result = join(files, " ", NULL);

  test_assert_string(result, "dir/file1.txt dir/file2.ini dir/file3.txt", "Should return filenames separated by the delimiter");
}

void test_get_default() {
  test_assert_string(get_default(NULL, "default"), "default", "Should return the default value when given NULL");
  test_assert_string(get_default("", "default"), "default", "Should return the default value when given empty string");
  test_assert_string(get_default("value", "default"), "value", "Should return the value when given non-empty value");
}

void test_is_lib() {
  test_assert(is_lib("libthing"), "Should return TRUE when string starts with lib");
  test_assert(!is_lib("thing"), "Should return FALSE when string does not start with lib");
}

void test_prefix() {
  test_assert_string(prefix("prefix.", "thing"), "prefix.thing", "Should join the strings");
}

static FileInfo *make_file(const char *name, const char *path, const char *ext) {
  FileInfo *file = (FileInfo *)malloc(sizeof(FileInfo));
  char *fullname = (char *)malloc(sizeof(char) * 1024);

  sprintf(fullname, "%s/%s.%s", path, name, ext);

  file->name = name;
  file->path = path;
  file->extension = ext;
  file->fullname = fullname;
  file->hasExtension = FALSE;
  file->isDirectory = FALSE;
  file->isFile = TRUE;

  return file;
}
