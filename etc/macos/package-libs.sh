#!/bin/bash
set -eu

echo "  Creating dev.smilne.cman.docs..."
mkdir pkgbuild/dev.smilne.cman.docs

cp -r $ROOT/doc/* pkgbuild/dev.smilne.cman.docs/

pkgbuild --quiet \
  --identifier dev.smilne.cman.docs \
  --version 0.1.1 \
  --install-location /usr/local/opt/cman \
  --root pkgbuild/dev.smilne.cman.docs \
  packages/dev.smilne.cman.docs.pkg 2>> "$ROOT/build.log"
