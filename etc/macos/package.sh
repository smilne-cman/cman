#!/bin/bash
set -eu

ROOT=$(pwd)
WORKSPACE="bin/macos"
INSTALLER_NAME="Install cman"
DISK_IMAGE_NAME="cman"

# If executable isn't built yet then build it
if [ ! -d dist ] || [ ! -f dist/cman-*.tar.gz ]; then
  cman
fi

if [ -d $WORKSPACE ]; then
  rm -fr $WORKSPACE
fi

# Create folders to hold packaging artifacts
mkdir $WORKSPACE
mkdir $WORKSPACE/packages
mkdir $WORKSPACE/pkgbuild

echo "Creating component packages..."
cd $WORKSPACE

echo "  Creating dev.smilne.cman..."
mkdir pkgbuild/dev.smilne.cman
mkdir pkgbuild/dev.smilne.cman/cache
cp $ROOT/bin/cman pkgbuild/dev.smilne.cman/cman

pkgbuild --quiet \
  --identifier dev.smilne.cman \
  --version 0.1.1 \
  --install-location /usr/local/opt/cman \
  --root pkgbuild/dev.smilne.cman \
  --scripts "$ROOT/etc/macos/packages/core/scripts" \
  packages/dev.smilne.cman.pkg 2>> "$ROOT/build.log"

. $ROOT/etc/macos/package-libs.sh
echo ""

echo "Creating distribution package..."
productbuild --quiet \
  --distribution "$ROOT/etc/macos/packages/installer/Distribution.xml" \
  --resources "$ROOT/etc/macos/packages/installer/resources" \
  --package-path "packages/" \
  "$INSTALLER_NAME.pkg" 2>> "$ROOT/build.log"

echo "Creating disk image..."
cp -r $ROOT/etc/macos/dmg .
cp "$INSTALLER_NAME.pkg" dmg/

hdiutil create \
  -srcfolder dmg \
  -quiet \
  -ov \
  -volname cman \
  -format UDRO \
  "$DISK_IMAGE_NAME.dmg" 2>> "$ROOT/build.log"

cp "$DISK_IMAGE_NAME.dmg" "$ROOT/dist/"
