<!DOCTYPE html>
<html lang="en">
  <head>
    <title>cman Manual</title>

    <link rel="stylesheet" href="styles.css" />
  </head>
  <body class="font-helvetica narrow">
    <header>
      <h1 class="align-center">cman Manual</h1>

      <nav class="nav" aria-label="Table of contents">
        <h2>Table of Contents</h2>
        <ol class="nav__list">
          <li>
            <a href="#section_installation">Installation</a>
            <ol type="a">
              <li><a href="#section_installation_mac">macOS</a></li>
              <li><a href="#section_installation_linux">Linux</a></li>
              <li><a href="#section_installation_windows">Windows</a></li>
              <li><a href="#section_installation_uninstall">Uninstall</a></li>
            </ol>
          </li>
          <li>
            <a href="#section_usage">Usage</a>
            <ol type="a">
              <li><a href="#section_usage_example">Example Project</a></li>
            </ol>
          </li>
        </ol>
      </nav>
    </header>

    <main>
      <p>
        This is the manual for the cman build and dependency management system for C based projects. You can use cman to scaffold a new project and install dependencies and compile the project. Cman was built to give C a more modern feel with easy package management, you can think of it as <a href="https://www.npmjs.com/">NPM</a> for C.
      </p>

      <h2 id="section_installation">Installation</h2>
      <p>
        You can install cman by downloading a pre-compiled binary or you can clone the repository and compile it yourself.  To install from a precompiled binary, download the correct version for your platform.  The macOS build has an installer to make installation easier.
      </p>

      <h3 id="section_installation_mac">macOS</h3>
      <p>
        To install cman on macOS, download the latest <code>cman-x.y.z.dmg</code> file and mount it. Once mounted you can run the included <code>Install cman.pkg</code> file which will walk you through the installation.
      </p>

      <h3 id="section_installation_linux">Linux</h3>
      <p>
        To install cman on Linux, download the latest <code>cman-x.y.z.tar.gz</code> file. Once downloaded you must decompress the file and move it into its install location. You can do this with the following shell commands;
      </p>

      <pre class="code-block"><code>$ gunzip cman-1.0.0.tar.gz
$ tar -xf cman-1.0.0.tar
$ mv cman /usr/local/opt/cman
$ ln -s /usr/local/opt/cman/cman /usr/local/bin</code></pre>

      <h3 id="section_installation_windows">Windows</h3>
      <p>
        Currently cman does not support windows.
      </p>

      <h3 id="section_usage_uninstall">Uninstall</h3>
      <p>
        Uninstalling cman is a manual process at the moment. To uninstall on both macOS and Linux type the following command into the terminal;
      </p>

      <pre class="code-block"><code>$ rm ~/.cmanrc
$ rm -fr ~/.cman
$ rm -fr /usr/local/bin/cman
$ rm -fr /usr/local/opt/cman</code></pre>

      <p>
        These commands will delete the user settings and cache for cman and then remove its symlink and installation directory.
      </p>

      <h2 id="section_usage">Usage</h2>
      <p>
        Once cman is installed it will be available as the <code>cman</code> command in the terminal. You can interact with cman by invoking it at the command line with arguments. To see all of the arguments that cman accepts, type the command <code>cman help</code> on the terminal.
      </p>

      <h3 id="section_usage_example">Example Project</h3>
      <p>
        This section contains a walkthrough of how to build a simple hello world program using cman. We will build a small hello world program which gets the users name and prints a colourful greeting.
      </p>

      <p>First we must scaffold a new project by creating a new directory to hold it and calling <code>cman init</code> like so;</p>

      <pre class="code-block"><code>~ $ mkdir helloworld↵
~ $ cd helloworld↵
~/helloworld $ cman init helloworld↵
CMan Version 1.0.0

Project Version (1.0.0): ↵
Project Description: My hello world↵

Creating new project helloworld...
Creating new file src/main.c...
Initialized empty Git repository in /Users/steve/helloworld/.git/
[master (root-commit) f521ae1] INITIAL COMMIT
 3 files changed, 28 insertions(+)
 create mode 100644 .gitignore
 create mode 100644 project.ini
 create mode 100644 src/main.c</code></pre>

      <p>
        The init command takes a few details about the project like its name, version and a simple description. You can just press enter to accept default values for these. In the example we used the default version of 1.0.0 but specified a custom description.
      </p>

      <p>
        Once cman has taken the project information from us it will start scaffolding the project. It creates a <code>project.ini</code> file which holds metadata about the project including its dependencies. It creates a stub <code>main.c</code> file for us to start writing our program in and it also creates a <a href="https://git-scm.com/">git</a> repository for us to commit to.
      </p>

      <p>
        Now that we have a project set up, we can install libprompt which we will use to interact with the user. You can install libraries by using the <code>cman install</code> command;
      </p>

      <pre class="code-block"><code>~/helloworld $ cman install libprompt↵
CMan Version 1.0.0

Installing libprompt@latest...
Installing libstring@1.0.2...
Installing libcollection@1.0.5...</code></pre>

      <p>
        The install command will install the latest (or a specific version) of a package and all of its dependencies for us. We can now use functions defined by libprompt in our own program.
      </p>

      <p>
        Now we will edit the generated <code>main.c</code> file and change its contents to be;
      </p>

      <pre class="code-block"><code>#include &lt;stdio.h&gt;
#include &lt;stdlib.h&gt;
#include &lt;libprompt/libprompt.h&gt;

/* Types **************************************************/

/* Prototypes *********************************************/

/* Global Variables ***************************************/

/* Functions **********************************************/
int main(int argc, char *argv[]) {
  const char *name = prompt("What is your name?", 80);

  char *greeting = (char *)malloc(sizeof(char) * 128);
  sprintf(greeting, "Hello %s!", name);

  rainbow(greeting);

  exit(0);
}</code></pre>

    <p>
      We have added an import for libprompt at the top so that we can access its functionality. You can view the package headers by looking in the <code>lib/</code> directory in the project root directory. We have used the <code>prompt()</code> function to retrieve the users name and the <code>rainbow()</code> function to display the multicolour greeting, these are both supplied by libprompt.
    </p>

    <p>
      Now that we are ready to run our program we must compile and link it. With cman this is easy, just run cman without any arguments and it'll know what to do.
    </p>

    <pre class="code-block"><code>~/helloworld $ cman↵
CMan Version 1.0.0

Cleaning project...
Compiling project...
  src/main.c...
Linking project...
Compiling unit tests...
Running unit tests...

Packaging project...
Installing symlink...</code></pre>

      <p>
        That one simple command will compile all of the code in your project and link it with the installed packages. If we had written unit tests then they would have also been run. The project is then packaged up into a <code>helloworld-1.0.0.tar.gz</code> file to be distributed with others.
      </p>

      <p>Runnning <code>tree</code> on the project will yield these files as the final result;</p>

      <pre class="code-block"><code>~/helloworld $ tree↵
.
├── bin
│   ├── helloworld
│   ├── main.o
│   └── tests
├── build.log
├── dist
│   └── helloworld-1.0.0.tar.gz
├── doc
├── lib
│   ├── libcollection
│   │   ├── all.h
│   │   ├── compare.h
│   │   ├── iterator.h
│   │   ├── libcollection.a
│   │   ├── library.ini
│   │   ├── list.h
│   │   ├── map.h
│   │   ├── printer.h
│   │   ├── set.h
│   │   └── types.h
│   ├── libprompt
│   │   ├── libprompt.a
│   │   ├── libprompt.h
│   │   └── library.ini
│   ├── libstring
│   │   ├── library.ini
│   │   ├── libstring.a
│   │   └── libstring.h
│   └── libtest
│       ├── library.ini
│       ├── libtest.a
│       └── libtest.h
├── project.ini
├── res
├── src
│   └── main.c
├── test
└── tmp
    └── logs
        └── main.log</code></pre>
    </main>
  </body>
</html>
