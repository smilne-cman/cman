# CMan - Dependency Management for C Projects
> Impregnating your C projects with dependencies

This repository contains a dependency and build management program that helps build other C programs in a style similar to Javascript with NPM. Using this you can install local libraries with syntax like `cman install libtest`.

## Usage
Once cman is installed you can invoke it anywhere in the terminal to create a brand new project in the current directory. If a project already exists in the directory then cman will allow you to install dependencies, build the project and run unit tests.

To create a new project just navigate to the directory where you want it to live and type the following;

```
$ cman init myproject
```

This will ask you some questions about the project you are building and scaffold a workspace for you and create the project entry point. Some useful libraries will be pre-installed for you which allow you to write unit tests. If you want to install more libraries then you can install them like so;

```
$ cman install libcollection 1.0.1
$ cman install libcommand
```

Once you've got the project workspace setup and some code written you can build the application and run the unit tests by just running the command `cman` within the root directory. This will compile your application and link it with the dependencies, run it's unit tests and then package it up for you.

## Building
If you are building cman from scratch then you can use the `build.sh` bash script to compile and run the unit tests. Once cman has been compiled it will be installed for you. After that you can build cman with itself.

## Licensing
This software is licensed using the MIT license allowing you to do pretty much whatever you want with it. For more information check out the `LICENSE` file in the root of this repository.
