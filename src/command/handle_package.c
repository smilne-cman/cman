#include <stdio.h>
#include <stdlib.h>
#include <libcollection/list.h>
#include <libcollection/types.h>
#include <libprompt/libprompt.h>
#include "handle_package.h"
#include "../actions/actions.h"
#include "../utility.h"
#include "../labels.h"
#include "../service/project.h"

/* Types **********************************************************************/

/* Prototypes *****************************************************************/

/* Global Variables ***********************************************************/

/* Functions ******************************************************************/
void on_package(List *arguments) {
  Project *project = get_project_service()->current();

	if (project == NULL) {
		warning(LABEL_PROJECT_NOT_EXISTS_ERROR);
		return;
	}

  if(project->isLibrary) {
    puts("Packaging library...");
    invoke(action_package_library());
  } else {
    puts("Packaging project...");
    invoke(action_package_project());
  }
}
