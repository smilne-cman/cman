#ifndef __handle_init__
#define __handle_init__

/* Includes *******************************************************************/
#include <libcollection/list.h>
#include "../service/project.h"

/* Types **********************************************************************/
typedef struct InitData {
	int isLibrary;
  char *name;
  char *version;
  char *description;
  ProjectType type;
} InitData;

/* Macros *********************************************************************/

/* Global Functions ***********************************************************/
InitData *resolve_init(List *arguments);
void on_init(InitData *data);

#endif
