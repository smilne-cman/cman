#include <stdio.h>
#include <stdlib.h>
#include <libcollection/list.h>
#include <libcollection/types.h>
#include <libprompt/libprompt.h>
#include "handle_test.h"
#include "../actions/actions.h"
#include "../utility.h"
#include "../labels.h"
#include "../service/project.h"

/* Types **********************************************************************/

/* Prototypes *****************************************************************/

/* Global Variables ***********************************************************/

/* Functions ******************************************************************/
void on_test(List *arguments) {
	if (get_project_service()->current() == NULL) {
		warning(LABEL_PROJECT_NOT_EXISTS_ERROR);
    return;
	}

	puts("Compiling unit tests...");
  invoke(action_compile_tests());

  puts("Running unit tests...");
  invoke(action_run_tests());
}
