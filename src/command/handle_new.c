#include <stdio.h>
#include <stdlib.h>
#include <libcollection/list.h>
#include <libcollection/types.h>
#include <libprompt/libprompt.h>
#include "handle_new.h"
#include "../actions/actions.h"
#include "../utility.h"
#include "../labels.h"
#include "../service/project.h"

/* Types **********************************************************************/

/* Prototypes *****************************************************************/
static int confirm_file(const char *path, char *module, char *filename, const char *ext);

/* Global Variables ***********************************************************/

/* Functions ******************************************************************/
NewData *resolve_new(List *arguments) {
  Project *project = get_project_service()->current();

  if(project == NULL) {
		warning(LABEL_PROJECT_NOT_EXISTS_ERROR);
    return NULL;
  }

	NewData *data = (NewData *)malloc(sizeof(NewData));

  if (list_size(arguments) > 1) {
    data->module = list_gets(arguments, 0);
    data->name = list_gets(arguments, 1);
  } else {
    data->name = get_or_prompt(arguments, 0, LABEL_NEW_FILENAME, 80, "filename");
    data->module = NULL;
  }

  data->createSource = confirm_file(project->paths->source, data->module, data->name, "c");
  data->createHeader = confirm_file(project->paths->source, data->module, data->name, "h");
  data->createTest = confirm_file(project->paths->tests, NULL, data->name, "test.c");

	return data;
}

void on_new(NewData *data) {
	invoke(action_new_file(data->name, data->module, data->createSource, data->createHeader, data->createTest));
}

static int confirm_file(const char *path, char *module, char *filename, const char *ext) {
	char *buffer = (char *)malloc(sizeof(char) * 255);

  if (module == NULL) {
    sprintf(buffer, "Create file %s/%s.%s?", path, filename, ext);
  } else {
    sprintf(buffer, "Create file %s/%s/%s.%s?", path, module, filename, ext);
  }

  int response = confirm(buffer, TRUE);

	free(buffer);
	return response;
}
