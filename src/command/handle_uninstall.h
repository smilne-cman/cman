#ifndef __handle_uninstall__
#define __handle_uninstall__

/* Includes *******************************************************************/
#include <libcollection/list.h>

/* Types **********************************************************************/

/* Macros *********************************************************************/

/* Global Functions ***********************************************************/
void on_uninstall(List *args);

#endif
