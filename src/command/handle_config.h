#ifndef __handle_config__
#define __handle_config__

/* Includes *******************************************************************/
#include <libcollection/list.h>

/* Types **********************************************************************/
typedef struct ConfigData {
  const char *name;
  const char *value;
} ConfigData;

/* Macros *********************************************************************/

/* Global Functions ***********************************************************/
ConfigData *resolve_config(List *args);
void on_config(ConfigData *data);

#endif
