#ifndef __handle_new__
#define __handle_new__

/* Includes *******************************************************************/
#include <libcollection/list.h>

/* Types **********************************************************************/
typedef struct NewData {
  char *name;
  char *module;
  int prompt;
  int createSource;
  int createHeader;
  int createTest;
} NewData;

/* Macros *********************************************************************/

/* Global Functions ***********************************************************/
NewData *resolve_new(List *arguments);
void on_new(NewData *data);

#endif
