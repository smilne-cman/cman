#include <stdio.h>
#include <stdlib.h>
#include <libcollection/list.h>
#include <libprompt/libprompt.h>
#include "handle_init.h"
#include "../utility.h"
#include "../labels.h"
#include "../constants.h"
#include "../actions/actions.h"
#include "../service/project.h"

/* Types **********************************************************************/

/* Prototypes *****************************************************************/
static ProjectType get_type();

/* Global Variables ***********************************************************/
static const char *DEFAULT_PROJECT_NAME = "untitled";
static const char *DEFAULT_PROJECT_VERSION = "1.0.0";
static const char *DEFAULT_PROJECT_DESCRIPTION = "Put a short description of your library here.";

/* Functions ******************************************************************/
InitData *resolve_init(List *arguments) {
	InitData *data = (InitData *)malloc(sizeof(InitData));

	data->name = get_or_prompt(arguments, 0, LABEL_NEW_PROJECT_NAME, PROJECT_NAME_MAX_LENGTH, DEFAULT_PROJECT_NAME);
	data->version = get_or_prompt(arguments, 1, LABEL_NEW_PROJECT_VERSION, PROJECT_VERSION_MAX_LENGTH, DEFAULT_PROJECT_VERSION);
	data->description = get_or_prompt(arguments, 2, LABEL_NEW_PROJECT_DESCRIPTION, PROJECT_DESCRIPTION_MAX_LENGTH, DEFAULT_PROJECT_DESCRIPTION);

	data->isLibrary = is_lib(data->name);
  data->type = data->isLibrary ? get_type() : StandardProject;

	return data;
}

void on_init(InitData *data) {
	invoke(action_create_project(data->name, data->version, data->description, data->type));
}

static ProjectType get_type() {
  List *types = list_new(ListString);
  list_adds(types, "static");
  list_adds(types, "dynamic");

  const char *chosen = choice(LABEL_NEW_PROJECT_TYPE, types, "static");

  list_destroy(types);
  if (!strcmp(chosen, "dynamic")) {
    return DynamicLibrary;
  } else {
    return StaticLibrary;
  }
}
