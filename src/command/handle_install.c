#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <libcollection/list.h>
#include <libcollection/set.h>
#include <libcollection/types.h>
#include <libprompt/libprompt.h>
#include "handle_install.h"
#include "../service/settings.h"
#include "../service/project.h"
#include "../labels.h"
#include "../actions/actions.h"
#include "../service/repository.h"

/* Types **********************************************************************/

/* Prototypes *****************************************************************/
static void install_latest(RepositoryService *service, char *name);
static void install_nested_dependencies(Project *dependency);

/* Global Variables ***********************************************************/

/* Functions ******************************************************************/
InstallData *resolve_install(List *args) {
  Project *project = get_project_service()->current();

  if(project == NULL) {
    error(LABEL_PROJECT_NOT_EXISTS_ERROR);
    return NULL;
  }

  if (list_size(args) == 0) {
    // Install all of this projects dependencies
    install_nested_dependencies(project);
    return NULL;
  }

  InstallData *data = (InstallData *)malloc(sizeof(InstallData));

  data->name = list_gets(args, 0);
  data->version = list_size(args) > 1 ? list_gets(args, 1) : NULL;
  data->phantom = FALSE;

  return data;
}

void on_install(InstallData *data) {
  RepositoryService *repository = get_repository_service();
  ProjectService *service = get_project_service();
  Project *project = service->current();

  if (!repository->exists(data->name)) {
    error(LABEL_LIBRARY_NOT_FOUND);
    return;
  }

  if (data->version == NULL) {
    install_latest(repository, data->name);
    return;
  }

  if (!repository->version_exists(data->name, data->version)) {
    error(LABEL_LIBRARY_VERSION_NOT_FOUND);
    return;
  }

  printf("Installing %s@%s...\n", data->name, data->version);
  repository->get(data->name, data->version, project->paths->library);

  install_nested_dependencies(service->load(data->name));

  if(!data->phantom) {
    service->add(data->name, data->version);
  }
}

static void install_latest(RepositoryService *repository, char *name) {
  printf("Installing %s@latest...\n", name);
  ProjectService *service = get_project_service();
  Project *project = service->current();

  repository->get(name, NULL, project->paths->library);

  install_nested_dependencies(service->load(name));

  service->add(name, repository->get_latest(name));
}

static void install_nested_dependencies(Project *dependency) {
  ProjectService *service = get_project_service();
  Iterator *iterator = set_iterator(dependency->libraries);

  while(has_next(iterator)) {
    Library *nestedDependency = (Library *)next(iterator);

    if(!service->installed(nestedDependency)) {
      InstallData *data = (InstallData *)malloc(sizeof(InstallData));

      data->name = nestedDependency->name;
      data->version = nestedDependency->version;
      data->phantom = TRUE;

      on_install(data);
      free(data);
    }
  }
}
