#include <stdio.h>
#include <stdlib.h>
#include <libcollection/list.h>
#include <libprompt/libprompt.h>
#include "handle_compile.h"
#include "../actions/actions.h"
#include "../labels.h"
#include "../service/project.h"

/* Types **********************************************************************/

/* Prototypes *****************************************************************/

/* Global Variables ***********************************************************/

/* Functions ******************************************************************/
void on_compile(List *arguments) {
  ProjectService *service = get_project_service();
  Project *project = service->current();

  if(project == NULL) {
    warning(LABEL_PROJECT_NOT_EXISTS_ERROR);
    return;
  }

  puts(project->isLibrary ? "Compiling library..." : "Compiling project...");
  invoke(action_compile_project());
}
