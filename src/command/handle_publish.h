#ifndef __handle_publish__
#define __handle_publish__

/* Includes *******************************************************************/
#include <libcollection/list.h>

/* Types **********************************************************************/

/* Macros *********************************************************************/

/* Global Functions ***********************************************************/
void on_publish(List *arguments);

#endif
