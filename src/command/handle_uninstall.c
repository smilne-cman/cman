#include <stdio.h>
#include <stdlib.h>
#include <libprompt/libprompt.h>
#include <libcollection/list.h>
#include "../service/project.h"
#include "../labels.h"
#include "handle_uninstall.h"

/* Types **********************************************************************/

/* Prototypes *****************************************************************/

/* Global Variables ***********************************************************/

/* Functions ******************************************************************/
void on_uninstall(List *args) {
  ProjectService *service = get_project_service();
  Project *project = service->current();

  if(project == NULL) {
    error(LABEL_PROJECT_NOT_EXISTS_ERROR);
    return;
  }

  if(list_size(args) == 0) {
    error(LABEL_UNINSTALL_NAME_REQUIRED);
    return;
  }

  Library *library = library_new(list_gets(args, 0), NULL);
	if(service->installed(library)) {
    printf("Removing %s...\n", library->name);
    char *install_dir = (char *)malloc(sizeof(char) * 1024);

    sprintf(install_dir, "%s/%s", project->paths->library, library->name);
    directory_remove(install_dir);
    free(install_dir);
  }

  printf("Uninstalling %s...\n", library->name);
  service->remove(library->name);
  free(library);
}
