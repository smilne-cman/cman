#ifndef __handle_install__
#define __handle_install__

/* Includes *******************************************************************/
#include <libcollection/list.h>

/* Types **********************************************************************/
typedef struct InstallData {
  char *name;
  char *version;
  int phantom;
} InstallData;

/* Macros *********************************************************************/

/* Global Functions ***********************************************************/
InstallData *resolve_install(List *args);
void on_install(InstallData *data);

#endif
