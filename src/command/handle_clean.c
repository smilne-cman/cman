#include <stdio.h>
#include <stdlib.h>
#include <libcollection/list.h>
#include <libprompt/libprompt.h>
#include "handle_clean.h"
#include "../actions/actions.h"
#include "../labels.h"
#include "../service/project.h"

/* Types **********************************************************************/

/* Prototypes *****************************************************************/

/* Global Variables ***********************************************************/

/* Functions ******************************************************************/
void on_clean(List *arguments) {
  ProjectService *service = get_project_service();

	if(service->current() == NULL) {
    warning(LABEL_PROJECT_NOT_EXISTS_ERROR);
    return;
  }

  invoke(action_clean_build());
}
