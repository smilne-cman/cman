#include <stdio.h>
#include <stdlib.h>
#include <libcollection/list.h>
#include <libcollection/types.h>
#include <libprompt/libprompt.h>
#include "handle_symlink.h"
#include "../actions/actions.h"
#include "../utility.h"
#include "../labels.h"
#include "../service/project.h"

/* Types **********************************************************************/

/* Prototypes *****************************************************************/

/* Global Variables ***********************************************************/

/* Functions ******************************************************************/
void on_symlink(List *arguments) {
	Project *project = get_project_service()->current();
	if(project == NULL) {
		warning(LABEL_PROJECT_NOT_EXISTS_ERROR);
    return;
  }

  if(!project->isLibrary) {
    puts("Installing symlink...");
    invoke(action_install_symlink());
  } else {
		warning("Cannot symlink a library");
	}
}
