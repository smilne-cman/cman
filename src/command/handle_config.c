#include <stdio.h>
#include <stdlib.h>
#include <libcollection/set.h>
#include <libcollection/iterator.h>
#include "handle_config.h"
#include "../service/settings.h"
#include "../utility.h"

/* Types **********************************************************************/

/* Prototypes *****************************************************************/
static void print_setting(const char *name);
static void show_settings();

/* Global Variables ***********************************************************/

/* Functions ******************************************************************/
ConfigData *resolve_config(List *args) {
  ConfigData *data = (ConfigData *)malloc(sizeof(ConfigData));

  data->name = list_gets(args, 0);
  data->value = list_gets(args, 1);

  return data;
}

void on_config(ConfigData *data) {
  if (data->name == NULL) {
    show_settings();
    return;
  }

  if (data->value == NULL) {
    print_setting(data->name);
    return;
  }

  SettingsService *settings = get_settings_service();

  settings->set(data->name, data->value);
  settings->save();

  printf("Saved setting '%s' = '%s'\n", data->name, data->value);
}

static void print_setting(const char *name) {
  SettingsService *settings = get_settings_service();
  const char *value = settings->get(name);

  if (value != NULL) {
    printf("%s = '%s'\n", name, value);
  }
}

static void show_settings() {
  SettingsService *settings = get_settings_service();

  Iterator *iterator = set_iterator(settings->list());
  while (has_next(iterator)) {
    const char *setting = next(iterator);

    printf("%s = '%s'\n", setting, settings->get(setting));
  }

  iterator_destroy(iterator);
}
