#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <libprompt/libprompt.h>
#include <libcollection/list.h>
#include "handle_publish.h"
#include "../service/cache.h"
#include "../service/project.h"
#include "../labels.h"

/* Types **********************************************************************/

/* Prototypes *****************************************************************/
static const char *get_cache_filename(const char *name, const char *version);

/* Global Variables ***********************************************************/

/* Functions ******************************************************************/
void on_publish(List *arguments) {
  Project *project = get_project_service()->current();

  if (project == NULL) {
    error(LABEL_PROJECT_NOT_EXISTS_ERROR);
    return;
  }

  get_cache_service()->put(
    project->name,
    project->version,
    project->author,
    project->paths->distribution,
    get_cache_filename(project->name, project->version)
  );

  puts("Published library to local cache");
}

static const char *get_cache_filename(const char *name, const char *version) {
  const char *buffer = (const char *)malloc(sizeof(char) * 1024);

  sprintf(buffer, "%s-%s.tar.gz", name, version);

  return buffer;
}
