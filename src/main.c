#include <stdio.h>
#include <stdlib.h>
#include <libcommand/libcommand.h>
#include <libcollection/list.h>
#include <libcollection/map.h>

#include "service/settings.h"
#include "service/project.h"
#include "commands.h"
#include "labels.h"

static int is_script(const char *name);
static void script_invoke(const char *command);

int main(int argc, char *argv[]) {
  printf("%s Version %s\n\n", LABEL_APPLICATION, LABEL_VERSION);
  system("echo > build.log \"\"");

  List *args = from_args(argc, argv);
  const char *command = list_gets(args, 0);

  if (command != NULL && is_script(command)) {
    script_invoke(command);
  } else {
    command_invoke(get_handler(), args);
  }

  get_settings_service()->save();
  puts("");
  exit(0);
}

static int is_script(const char *name) {
  Project *project = get_project_service()->current();

  return project != NULL && map_contains(
    project->scripts, name
  );
}

static void script_invoke(const char *command) {
  Map *scripts = get_project_service()->current()->scripts;

  const char *script = (const char *)map_get(scripts, command);
  system(script);
}
