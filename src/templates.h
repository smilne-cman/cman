#ifndef __TEMPLATES__
#define __TEMPLATES__

/* Includes *******************************************************************/
#include <libcollection/list.h>

/* Types **********************************************************************/

/* Macros *********************************************************************/

/* Global Functions ***********************************************************/
void template_gitignore(List *paths);
void template_source(const char *path, const char *module, const char *name);
void template_source_main(const char *path, const char *name);
void template_header(const char *path, const char *module, const char *name);
void template_test(const char *path, const char *module, const char *name);

#endif
