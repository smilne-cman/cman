#include <stdio.h>
#include <stdlib.h>
#include "utility.h"

#include <libcollection/list.h>
#include <libcollection/types.h>
#include <libcollection/iterator.h>
#include <libprompt/libprompt.h>
#include <libfiles/libfiles.h>

/* Types **********************************************************************/

/* Prototypes *****************************************************************/

/* Global Variables ***********************************************************/

/* Functions ******************************************************************/

char *join(List *files, const char *delimiter, const char *prefix) {
	char *buffer = (char *)malloc(sizeof(char) * 2048);
	sprintf(buffer, "");

	int first = TRUE;
	Iterator *iterator = list_iterator(files);
	while(has_next(iterator)) {
		FileInfo *file = (FileInfo *)next(iterator);

		if (first) {
			sprintf(buffer, "%s%s", prefix == NULL ? "" : prefix, file->fullname);
			first = FALSE;
		} else {
			sprintf(buffer, "%s%s%s%s", buffer, delimiter, prefix == NULL ? "" : prefix, file->fullname);
		}
	}

	iterator_destroy(iterator);
	return buffer;
}

char *get_default(char *value, char *defaultValue) {
  if(value == NULL || strlen(value) <= 1) return defaultValue;

  return value;
}

int is_lib(char *name) {
  if(name == NULL) return FALSE;

  return strstr(name, "lib") != NULL;
}

char *get_or_prompt(List *list, int index, char *label, int length, char *def) {
	return list_size(list) >= index + 1
		? list_gets(list, index)
		: get_default(prompt(label, length), def);
}

char *prefix(char *prefix, char *value) {
  const char *buffer = (const char *)malloc(sizeof(char) * 1024);

  sprintf(buffer, "%s%s", prefix, value);

  return buffer;
}
