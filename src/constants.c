#include <stdio.h>
#include <stdlib.h>
#include "constants.h"

/* Types **********************************************************************/

/* Prototypes *****************************************************************/

/* Global Variables ***********************************************************/
int PROJECT_NAME_MAX_LENGTH = 120;
int PROJECT_VERSION_MAX_LENGTH = 80;
int PROJECT_DESCRIPTION_MAX_LENGTH = 200;

#if defined(__linux__) || defined(unix) || defined(__unix__) || defined(__unix)
  const char EXTENSION_STATIC_LIBRARY[] = "a";
  const char EXTENSION_DYNAMIC_LIBRARY[] = "so";
#endif

#if defined(__APPLE__)
  const char EXTENSION_STATIC_LIBRARY[] = "a";
  const char EXTENSION_DYNAMIC_LIBRARY[] = "dylib";
#endif

#if defined(_WIN32) || defined(_WIN64)
  const char EXTENSION_STATIC_LIBRARY[] = "lib";
  const char EXTENSION_DYNAMIC_LIBRARY[] = "dll";
#endif

/* Functions ******************************************************************/
