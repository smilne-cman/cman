#ifndef __settings__
#define __settings__

/* Includes *******************************************************************/
#include <libcollection/set.h>

/* Types **********************************************************************/
typedef struct SettingsService {
  const char *(*get)(const char *name);
  void (*set)(const char *name, const char *value);
  void (*save)();
  Set *(*list)();
} SettingsService;

/* Macros *********************************************************************/

/* Global Functions ***********************************************************/
SettingsService *get_settings_service();
void set_settings_service(SettingsService *service);

const char *settings_get(const char *name);
void settings_set(const char *name, const char *value);
void settings_save();
Set *settings_list();

#endif
