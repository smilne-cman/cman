#ifndef __project__
#define __project__

/* Includes *******************************************************************/
#include <libini/libini.h>
#include <libcollection/set.h>

/* Types **********************************************************************/
typedef enum ProjectType {
  StandardProject,
  StaticLibrary,
  DynamicLibrary
} ProjectType;

typedef struct ProjectPaths {
  const char *distribution;
  const char *resources;
  const char *tests;
  const char *source;
  const char *binary;
  const char *library;
  const char *temporary;
  const char *documentation;
} ProjectPaths;

typedef struct CompilerArguments {
  const char *flags;
} CompilerArguments;

typedef struct Library {
  const char *name;
  const char *version;
} Library;

typedef struct Project {
  IniFile *configuration;
  ProjectPaths *paths;
  CompilerArguments *compiler;
  Set *libraries;
  Map *scripts;
  const char *name;
  const char *version;
  const char *author;
  int isLibrary;
  ProjectType type;
} Project;

typedef struct ProjectService {
  Project *(*current)();
  Project *(*load)(char *name);
  void (*add)(char *name, char *version);
  void (*remove)(char *name);
  int (*installed)(Library *library);
} ProjectService;

/* Macros *********************************************************************/

/* Global Functions ***********************************************************/
ProjectService *get_project_service();
void set_project_service(ProjectService *service);

Library *library_new(const char *name, const char *version);

Project *project_current();
Project *project_load(char *name);
void project_add(char *name, char *version);
void project_remove(char *name);
int project_is_installed(Library *library);

#endif
