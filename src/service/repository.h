#ifndef __repository__
#define __repository__

/* Includes *******************************************************************/

/* Types **********************************************************************/
typedef struct RepositoryService {
  int (*exists)(char *name);
  int (*version_exists)(char *name, char *version);
  void (*get)(char *name, char *version, char *path);
  void (*put)(char *name, char *version, char *author, char *path, char *filename);
  char *(*get_latest)(char *name);
} RepositoryService;
/* Macros *********************************************************************/

/* Global Functions ***********************************************************/
RepositoryService *get_repository_service();

int repository_exists(char *name);
int repository_version_exists(char *name, char *version);
void repository_get(char *name, char *version, char *path);
void repository_put(char *name, char *version, char *author, char *path, char *filename);
char *repository_get_latest(char *name);

#endif
