#include <stdio.h>
#include <stdlib.h>
#include <libcollection/types.h>
#include "repository.h"
#include "cache.h"

/* Types **********************************************************************/

/* Prototypes *****************************************************************/

/* Global Variables ***********************************************************/

/* Functions ******************************************************************/
RepositoryService *get_repository_service() {
  RepositoryService *service = (RepositoryService *)malloc(sizeof(RepositoryService));

  service->exists = repository_exists;
  service->get = repository_get;
  service->get_latest = repository_get_latest;
  service->put = repository_put;
  service->version_exists = repository_version_exists;

  return service;
}

int repository_exists(char *name) {
  CacheService *cache = get_cache_service();
  if (cache->exists(name)) {
    free(cache);
    return TRUE;
  }

  free(cache);

  // TODO -- Check the remote repository

  return FALSE;
}

int repository_version_exists(char *name, char *version) {
  CacheService *cache = get_cache_service();
  if (cache->version_exists(name, version)) {
    free(cache);
    return TRUE;
  }

  free(cache);

  // TODO -- Check the remote repository

  return FALSE;
}

void repository_get(char *name, char *version, char *path) {
  CacheService *cache = get_cache_service();
  if (cache->exists(name)) {
    cache->get(name, version, path);
    free(cache);

    return;
  }

  // TODO -- Fetch from remote repository

  free(cache);
}

void repository_put(char *name, char *version, char *author, char *path, char *filename) {
  CacheService *cache = get_cache_service();
  cache->put(name, version, author, path, filename);

  free(cache);

  // TODO -- Push to remote repository
}

char *repository_get_latest(char *name) {
  CacheService *cache = get_cache_service();
  if (cache->exists(name)) {
    char *latest = cache->get_latest(name);
    free(cache);

    return latest;
  }

  // TODO -- Get latest version from remote repository

  free(cache);
  return NULL;
}
