#include <stdio.h>
#include <stdlib.h>

/* Types **********************************************************************/

/* Prototypes *****************************************************************/

/* Global Variables ***********************************************************/
const char LABEL_APPLICATION[] = "CMan";
const char LABEL_VERSION[] = "1.0.0";

const char LABEL_HELP[] =
  "  init [name] [version] [description]        -  Creates a new project\n"
  "  install [library] [version]                -  Downloads and installs dependencies\n"
  "  uninstall <library>                        -  Uninstalls a dependency\n"
  "  new [module] <name>                        -  Templates a new file\n"
  "  clean                                      -  Deletes old build artifacts\n"
  "  resources                                  -  Prepares the resources to be bundled with the executable\n"
  "  compile                                    -  Compiles the source code into object code\n"
  "  test                                       -  Runs unit tests against the compiled project\n"
  "  package                                    -  Packages and archives the executable for distribution\n"
  "  symlink                                    -  Installs a symlink in '/usr/local/bin\n"
  "  publish                                    -  Publish a library to a remote repository\n"
  "  config <setting> [value]                   -  Gets or sets a global setting value";

const char LABEL_NEW_PROJECT_NAME[] = "Project Name (untitled):";
const char LABEL_NEW_PROJECT_VERSION[] = "Project Version (1.0.0):";
const char LABEL_NEW_PROJECT_DESCRIPTION[] = "Project Description:";
const char LABEL_NEW_PROJECT_TYPE[] = "Library Type";

const char LABEL_NEW_FILENAME[] = "Filename (filename):";

const char LABEL_PROJECT_EXISTS_ERROR[] = "Project already exists in this directory!";
const char LABEL_PROJECT_NOT_EXISTS_ERROR[] = "Project not found!";

const char LABEL_LIBRARY_NAME_REQUIRED[] = "Must specify a library name to install.";
const char LABEL_LIBRARY_NOT_FOUND[] = "Library does not exist!";
const char LABEL_LIBRARY_VERSION_NOT_FOUND[] = "Library version does not exist!";

const char LABEL_INITIALISING_CACHE[] = "Initialising local cache...";

const char LABEL_UNINSTALL_NAME_REQUIRED[] = "Must specify a library name to uninstall.";

const char LABEL_SETTING_NOT_FOUND[] = "Setting not found!";

/* Functions ******************************************************************/
