#ifndef __LABELS__
#define __LABELS__

/* Includes *******************************************************************/

/* Types **********************************************************************/

/* Macros *********************************************************************/

/* Global Variables ***********************************************************/
extern const char LABEL_APPLICATION[];
extern const char LABEL_VERSION[];
extern const char LABEL_HELP[];

extern const char LABEL_NEW_PROJECT_NAME[];
extern const char LABEL_NEW_PROJECT_VERSION[];
extern const char LABEL_NEW_PROJECT_DESCRIPTION[];
extern const char LABEL_NEW_PROJECT_TYPE[];

extern const char LABEL_NEW_FILENAME[];

extern const char LABEL_PROJECT_EXISTS_ERROR[];
extern const char LABEL_PROJECT_NOT_EXISTS_ERROR[];

extern const char LABEL_LIBRARY_NAME_REQUIRED[];
extern const char LABEL_LIBRARY_NOT_FOUND[];
extern const char LABEL_LIBRARY_VERSION_NOT_FOUND[];

extern const char LABEL_INITIALISING_CACHE[];

extern const char LABEL_UNINSTALL_NAME_REQUIRED[];

extern const char LABEL_SETTING_NOT_FOUND[];

#endif
