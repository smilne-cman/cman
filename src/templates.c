#include <stdio.h>
#include <stdlib.h>
#include <libcollection/iterator.h>
#include "templates.h"

/* Types **********************************************************************/

/* Prototypes *****************************************************************/

/* Global Variables ***********************************************************/

/* Functions ******************************************************************/
void template_gitignore(List *paths) {
  FILE *file = fopen(".gitignore", "w");

  Iterator *iterator = list_iterator(paths);
  while(has_next(iterator)) {
    fprintf(file, "%s\n", ((const char *)next(iterator)));
  }

  iterator_destroy(iterator);
  fclose(file);
}

void template_source(const char *path, const char *module, const char *name) {
  char *filename = (char *)malloc(sizeof(char) * 80);

  if (module == NULL) {
    sprintf(filename, "%s/%s.c", path, name);
  } else {
    sprintf(filename, "%s/%s/%s.c", path, module, name);
  }

  FILE *file = fopen(filename, "w");

  fprintf(file, "#include <stdio.h>\n");
  fprintf(file, "#include <stdlib.h>\n");
  fprintf(file, "#include \"%s.h\"\n\n", name);

  fprintf(file, "/* Types **********************************************************************/\n\n");
  fprintf(file, "/* Prototypes *****************************************************************/\n\n");
  fprintf(file, "/* Global Variables ***********************************************************/\n\n");
  fprintf(file, "/* Functions ******************************************************************/\n\n");

  fclose(file);
  free(filename);
}

void template_source_main(const char *path, const char *name) {
  char *filename = (char *)malloc(sizeof(char) * 80);
  sprintf(filename, "%s/%s.c", path, name);

  FILE *file = fopen(filename, "w");

  fprintf(file, "#include <stdio.h>\n");
  fprintf(file, "#include <stdlib.h>\n\n");

  fprintf(file, "/* Types **********************************************************************/\n\n");
  fprintf(file, "/* Prototypes *****************************************************************/\n\n");
  fprintf(file, "/* Global Variables ***********************************************************/\n\n");
  fprintf(file, "/* Functions ******************************************************************/\n");
  fprintf(file, "int main(int argc, char *argv[]) {\n");
  fprintf(file, "  // TODO -- Implement\n");
  fprintf(file, "  exit(0);\n");
  fprintf(file, "}\n");

  fclose(file);
  free(filename);
}

void template_header(const char *path, const char *module, const char *name) {
  char *filename = (char *)malloc(sizeof(char) * 80);

  if (module == NULL) {
    sprintf(filename, "%s/%s.h", path, name);
  } else {
    sprintf(filename, "%s/%s/%s.h", path, module, name);
  }

  FILE *file = fopen(filename, "w");

  fprintf(file, "#ifndef __%s__\n", name);
  fprintf(file, "#define __%s__\n\n", name);

  fprintf(file, "/* Includes *******************************************************************/\n\n");
  fprintf(file, "/* Types **********************************************************************/\n\n");
  fprintf(file, "/* Macros *********************************************************************/\n\n");
  fprintf(file, "/* Global Functions ***********************************************************/\n\n");
  fprintf(file, "#endif\n");

  fclose(file);
  free(filename);
}

void template_test(const char *path, const char *module, const char *name) {
  char *filename = (char *)malloc(sizeof(char) * 80);
  sprintf(filename, "%s/%s.test.c", path, name);

  FILE *file = fopen(filename, "w");

  fprintf(file, "#include <stdio.h>\n");
  fprintf(file, "#include <stdlib.h>\n");
  fprintf(file, "#include <libtest/libtest.h>\n");

  if (module == NULL) {
    fprintf(file, "#include \"%s.h\"\n\n", name);
  } else {
    fprintf(file, "#include \"%s/%s.h\"\n\n", module, name);
  }

  fprintf(file, "/* Types **********************************************************************/\n\n");
  fprintf(file, "/* Prototypes *****************************************************************/\n");
  fprintf(file, "void test_method();\n\n");
  fprintf(file, "/* Global Variables ***********************************************************/\n\n");
  fprintf(file, "int main(int argc, char *argv[]) {\n");
  fprintf(file, "  test_init(\"%s\", argc, argv);\n\n", name);
  fprintf(file, "  test(\"Test Method\", test_method);\n\n");
  fprintf(file, "  return test_complete();\n");
  fprintf(file, "}\n\n");
  fprintf(file, "void test_method() {\n");
  fprintf(file, "  // TODO -- Write test\n");
  fprintf(file, "}\n");

  fclose(file);
  free(filename);
}
