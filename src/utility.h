#ifndef __utility__
#define __utility__

/* Includes *******************************************************************/
#include <libcollection/list.h>

/* Types **********************************************************************/

/* Macros *********************************************************************/

/* Global Functions ***********************************************************/
char *join(List *files, const char *delimiter, const char *prefix);
char *get_default(char *value, char *defaultValue);
int is_lib(char *name);
char *get_or_prompt(List *list, int index, char *prompt, int length, char *def);
char *prefix(char *prefix, char *value);

#endif
