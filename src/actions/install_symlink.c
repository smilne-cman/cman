#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "actions.h"

#include "actions.h"
#include "../labels.h"
#include "../templates.h"
#include "../service/settings.h"
#include "../service/project.h"

/* Types **********************************************************************/

/* Prototypes *****************************************************************/

/* Global Variables ***********************************************************/

/* Functions ******************************************************************/
Action *action_install_symlink() {
  Action *action = (Action *)malloc(sizeof(Action));

  action->type = ActionInstallSymlink;

  return action;
}

void install_symlink() {
  ProjectService *service = get_project_service();
  Project *project = service->current();
  SettingsService *settings = get_settings_service();

  if(project->isLibrary) {
    puts("Cannot create symlink to library.");
    return;
  }

  char *command = (char *)malloc(sizeof(char) * 1024);
  sprintf(
    command,
    "cp %s/%s %s/%s",
    project->paths->binary,
    project->name,
    settings->get("install-dir"),
    project->name
  );

  system(command);
  free(command);
}
