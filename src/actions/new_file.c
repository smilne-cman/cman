#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <libfiles/libfiles.h>

#include "actions.h"
#include "../labels.h"
#include "../templates.h"

/* Types **********************************************************************/

/* Prototypes *****************************************************************/
static void create_source(ProjectPaths *paths, char *module, char *name);
static void create_header(ProjectPaths *paths, char *module, char *name);
static void create_test(ProjectPaths *paths, char *module, char *name);
static char *get_module_path(const char *path, const char *module);

/* Global Variables ***********************************************************/

/* Functions ******************************************************************/
Action *action_new_file(char *name, char *module, int createSource, int createHeader, int createTest) {
  ActionNewFileData *data = (ActionNewFileData *)malloc(sizeof(ActionNewFileData));
  data->name = name;
  data->module = module;
  data->createSource = createSource;
  data->createHeader = createHeader;
  data->createTest = createTest;

  Action *action = (Action *)malloc(sizeof(Action));
  action->type = ActionNewFile;
  action->data = data;

  return action;
}

void new_file(ActionNewFileData *data) {
  Project *project = get_project_service()->current();
  if(project == NULL) {
    puts(LABEL_PROJECT_NOT_EXISTS_ERROR);
    return;
  }

  if (data->module != NULL) {
    char *path = get_module_path(project->paths->source, data->module);

    if (!directory_exists(path)) {
      directory_new(path);
    }

    free(path);
  }

  ProjectPaths *paths = project->paths;
  if(data->createSource) create_source(paths, data->module, data->name);
  if(data->createHeader) create_header(paths, data->module, data->name);
  if(data->createTest) create_test(paths, data->module, data->name);
}

static void create_source(ProjectPaths *paths, char *module, char *name) {
  printf("Creating new file %s/%s.c...\n", paths->source, name);

  if(strcmp(name, "main") == EQUAL) {
    template_source_main(paths->source, name);
  } else {
    template_source(paths->source, module, name);
  }
}

static void create_header(ProjectPaths *paths, char *module, char *name) {
  printf("Creating new file %s/%s.h...\n", paths->source, name);
  template_header(paths->source, module, name);
}

static void create_test(ProjectPaths *paths, char *module, char *name) {
  printf("Creating new file %s/%s.test.c...\n", paths->tests, name);
  template_test(paths->tests, module, name);
}

static char *get_module_path(const char *path, const char *module) {
  char *buffer = (char *)malloc(sizeof(char) * 1024);

  sprintf(buffer, "%s/%s", path, module);

  return buffer;
}
