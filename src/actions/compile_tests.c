#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "actions.h"

#include <libcollection/types.h>
#include <libcollection/list.h>
#include <libcollection/iterator.h>
#include <libfiles/libfiles.h>
#include <libprompt/libprompt.h>
#include "actions.h"
#include "../labels.h"
#include "../utility.h"
#include "../templates.h"
#include "../constants.h"

/* Types **********************************************************************/

/* Prototypes *****************************************************************/
static void delete_dir(const char *dir);

/* Global Variables ***********************************************************/

/* Functions ******************************************************************/
Action *action_compile_tests() {
  Action *action = (Action *)malloc(sizeof(Action));

  action->type = ActionCompileTests;

  return action;
}

void compile_tests() {
  Project *project = get_project_service()->current();
  ProjectPaths *paths = project->paths;

  char *testPath = file_prefix(paths->binary, "tests", "/");
  char *mainObj = file_prefix(paths->binary, "main.o", "/");
  char *mainObjDummy = file_prefix(paths->binary, "main.dummy", "/");

  directory_new(testPath);
  file_move(mainObj, mainObjDummy);
  chdir(testPath);

  const char *command = (const char *)malloc(sizeof(char) * 4096);
  sprintf(command, "cp ../*.%s ./ 2>> ../../build.log", EXTENSION_DYNAMIC_LIBRARY);
  system(command);

  const char *path = (const char *)malloc(sizeof(char) * 1024);
  sprintf(path, "../../%s", paths->tests);
  char *objects = join(files_by_extension("..", "o", TRUE, FALSE), " ", NULL);
  char *libs = join(files_by_extension(file_prefix("../..", paths->library, "/"), "a", TRUE, FALSE), " ", NULL);

  Iterator *iterator = directory_iterator(path, path, TRUE);
  while(has_next(iterator)) {
    FileInfo *file = next_file(iterator);

    if (file->isFile && ends_with(file->fullname, ".test.c")) {
      system("echo >> ../../build.log \"====> Compiling Unit Tests...\"");
      sprintf(command, "gcc -I../../%s -I../../%s -o %s.test %s %s %s 2>> ../../build.log", paths->source, paths->library, file->name, file->fullname, objects, libs);
      if(system(command)) {
        error("Error compiling unit tests!");
        printf("\n%s\n\n", command);
        exit(1);
      }

    }
  }

  iterator_destroy(iterator);
  chdir("../..");
  file_move(mainObjDummy, mainObj);

  free(libs);
  free(objects);
  free(path);
  free(mainObjDummy);
  free(command);
  free(mainObj);
  free(testPath);
}
