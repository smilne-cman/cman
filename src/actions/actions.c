#include <stdio.h>
#include <stdlib.h>
#include "actions.h"

/* Types **********************************************************************/

/* Prototypes *****************************************************************/

/* Global Variables ***********************************************************/

/* Functions ******************************************************************/
void invoke(Action *action) {
  switch (action->type) {
    case ActionCreateProject: return create_project((ActionCreateProjectData *)action->data);
    case ActionNewFile: return new_file((ActionNewFileData *)action->data);
    case ActionCleanBuild: return clean_build();
    case ActionCompileProject: return compile_project();
    case ActionLinkProject: return link_project();
    case ActionCompileTests: return compile_tests();
    case ActionRunTests: return run_tests();
    case ActionPackageProject: return package_project();
    case ActionInstallSymlink: return install_symlink();
    case ActionPackageLibrary: return package_library();
  }
}
