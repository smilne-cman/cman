#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "actions.h"

#include <libcollection/iterator.h>
#include <libcollection/types.h>
#include <libfiles/libfiles.h>
#include "actions.h"
#include "../service/settings.h"
#include "../service/project.h"

/* Types **********************************************************************/

/* Prototypes *****************************************************************/

/* Global Variables ***********************************************************/

/* Functions ******************************************************************/
Action *action_run_tests() {
  Action *action = (Action *)malloc(sizeof(Action));

  action->type = ActionRunTests;

  return action;
}

void run_tests() {
  ProjectService *service = get_project_service();
  SettingsService *settings = get_settings_service();
  Project *project = service->current();

  ProjectPaths *paths = project->paths;

  puts("");
  system("echo >> build.log \"====> Running Unit Tests...\"");
  char *testPath = file_prefix(paths->binary, "tests", "/");
  chdir(testPath);

  Iterator *iterator = directory_iterator(".", "", FALSE);
  while(has_next(iterator)) {
    FileInfo *file = next_file(iterator);

    if(file->hasExtension && !strcmp(file->extension, "test")) {
      char *command = (char *)malloc(sizeof(char) * 1024);
      sprintf(command, "./%s %s 2>> ../../build.log", file->fullname, settings->get("test-arguments"));
      if(system(command)) {
        system("cat ../../build.log");
        printf("\nUnit tests failed!");
        exit(1);
      };
      free(command);
    }
  }

  iterator_destroy(iterator);
  chdir("../..");
}
