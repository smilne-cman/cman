#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <libcollection/list.h>
#include <libcollection/types.h>
#include <libcollection/iterator.h>
#include <libfiles/libfiles.h>
#include <libprompt/libprompt.h>
#include "../constants.h"
#include "../utility.h"
#include "actions.h"

/* Types **********************************************************************/

/* Prototypes *****************************************************************/
static List *get_source_files();
static void compile_files(Project *project, List *files);
static void create_temp_dirs();
static void handle_errors(List *errors);
static gatherDynamicLibraries(Project *project);

/* Global Variables ***********************************************************/

/* Functions ******************************************************************/
Action *action_compile_project() {
  Action *action = (Action *)malloc(sizeof(Action));

  action->type = ActionCompileProject;

  return action;
}

void compile_project() {
  Project *project = get_project_service()->current();
  create_temp_dirs();

  system("echo >> build.log \"====> Compiling Project...\"");

  directory_new(project->paths->binary);
  gatherDynamicLibraries(project);
  chdir(project->paths->binary);

  compile_files(project, get_source_files(project));

  chdir("..");

  invoke(action_link_project());
}

static List *get_source_files(Project *project) {
  const char *path = (const char *)malloc(sizeof(char) * 1024);
  sprintf(path, "../%s", project->paths->source);

  List *files = files_by_extension(path, "c", TRUE, FALSE);

  free(path);
  return files;
}

static void compile_files(Project *project, List *files) {
  const char *command = (const char *)malloc(sizeof(char) * 4096);
  Iterator *iterator = list_iterator(files);
  List *errors = list_new(ListString);

  while(has_next(iterator)) {
    FileInfo *file = next_file(iterator);

    sprintf(command, "  %s...", file->fullname + 3);
    puts(command);

    sprintf(command, "echo >> ../build.log \"==> Compiling %s\"", file->fullname);
    system(command);

    if (project->type == DynamicLibrary) {
      sprintf(command, "gcc -c -fPIC %s %s -I../%s -I../%s 2>> ../tmp/logs/%s.log", project->compiler->flags, file->fullname, project->paths->source, project->paths->library, file->name);
    } else {
      sprintf(command, "gcc -c %s %s -I../%s -I../%s 2>> ../tmp/logs/%s.log", project->compiler->flags, file->fullname, project->paths->source, project->paths->library, file->name);
    }

    if(system(command)) {
      sprintf(command, "cat ../tmp/logs/%s.log >> ../build.log", file->name);
      system(command);

      list_adds(errors, file->name);
    }

    sprintf(command, "cat ../tmp/logs/%s.log >> ../build.log", file->name);
    system(command);
  }

  if (list_size(errors) > 0) {
    handle_errors(errors);
  }

  free(command);
  iterator_destroy(iterator);
  list_destroy(files);
  list_destroy(errors);
}

static void create_temp_dirs() {
  directory_new("tmp");
  directory_new("tmp/logs");
}

static void handle_errors(List *errors) {
  const char *buffer = (const char *)malloc(sizeof(char) * 2048);
  Iterator *iterator = list_iterator(errors);

  while(has_next(iterator)) {
    puts("");

    char *filename = (char *)next(iterator);
    sprintf(buffer, "%s.c:", filename);
    error(buffer);

    sprintf(buffer, "cat ../tmp/logs/%s.log", filename);
    system(buffer);
  }

  puts("");
  sprintf(buffer, "Errors found in %i files.", list_size(errors));
  error(buffer);
  exit(1);
}

static gatherDynamicLibraries(Project *project) {
  ProjectPaths *paths = project->paths;

  List *files = files_by_extension(paths->library, EXTENSION_DYNAMIC_LIBRARY, TRUE, TRUE);
  char *command = (const char *)malloc(sizeof(char) * 4096);
  Iterator *iterator = list_iterator(files);

  while(has_next(iterator)) {
    FileInfo *file = next_file(iterator);

    sprintf(command, "cp %s/%s %s/%s.%s", paths->library, file->fullname, paths->binary, file->name, EXTENSION_DYNAMIC_LIBRARY);
    system(command);
  }

  iterator_destroy(iterator);
  list_destroy(files);
  free(command);
}
