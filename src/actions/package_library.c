#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "actions.h"

#include <libcollection/iterator.h>
#include <libcollection/types.h>
#include <libfiles/libfiles.h>
#include "actions.h"
#include "../labels.h"
#include "../utility.h"
#include "../templates.h"
#include "../constants.h"

/* Types **********************************************************************/

/* Prototypes *****************************************************************/
static void packageStatic(Project *project, ProjectPaths *paths);
static void packageDynamic(Project *project, ProjectPaths *paths);
static void packageHeaders(Project *project, ProjectPaths *paths);
static void createDistribution(Project *project, ProjectPaths *paths);
static void extractDependencies(Project *project);

/* Global Variables ***********************************************************/

/* Functions ******************************************************************/
Action *action_package_library() {
  Action *action = (Action *)malloc(sizeof(Action));

  action->type = ActionPackageLibrary;

  return action;
}

void package_library() {
  Project *project = get_project_service()->current();

  directory_new(project->paths->distribution);

  if (project->type == DynamicLibrary) {
    packageDynamic(project, project->paths);
  } else {
    packageStatic(project, project->paths);
  }

  char *command = (char *)malloc(sizeof(char) * 4096);
  sprintf(command, "cp library.ini %s/library.ini", project->paths->distribution);
  system(command);
  free(command);

  packageHeaders(project, project->paths);
  createDistribution(project, project->paths);
}

static void packageStatic(Project *project, ProjectPaths *paths) {
  system("echo >> build.log \"====> Extracting and merging dependencies...\"");

  chdir(paths->binary);
  extractDependencies(project);

  system("echo >> ../build.log \"====> Packaging library...\"");

  char *objects = join(files_by_extension(".", "o", FALSE, FALSE), " ",  NULL);

  char *command = (char *)malloc(sizeof(char) * 4096);
  sprintf(command, "ar rcs %s.a %s", project->name, objects);
  system(command);

  chdir("..");
  sprintf(command, "cp %s/%s.a %s/%s.a", paths->binary, project->name, paths->distribution, project->name);
  system(command);

  free(objects);
  free(command);
}

static void packageDynamic(Project *project, ProjectPaths *paths) {
  system("echo >> build.log \"====> Packaging library...\"");
  chdir(paths->binary);

  char *objects = join(files_by_extension(".", "o", FALSE, FALSE), " ", NULL);
  char *libs = join(
    files_by_extension(
      prefix("../", paths->library),
      EXTENSION_STATIC_LIBRARY,
      TRUE,
      TRUE
    ),
    " ",
    prefix(
      prefix("../", paths->library),
      "/"
    )
  );

  char *command = (char *)malloc(sizeof(char) * 4096);
  sprintf(command, "gcc -shared -o %s-%s.%s %s %s",
    project->name,
    project->version,
    EXTENSION_DYNAMIC_LIBRARY,
    objects,
    libs
  );
  system(command);

  chdir("..");
  sprintf(command, "cp %s/%s-%s.%s %s/%s-%s.%s",
    paths->binary,
    project->name,
    project->version,
    EXTENSION_DYNAMIC_LIBRARY,
    paths->distribution,
    project->name,
    project->version,
    EXTENSION_DYNAMIC_LIBRARY
  );
  system(command);
}

static void packageHeaders(Project *project, ProjectPaths *paths) {
  char *command = (char *)malloc(sizeof(char) * 4096);
  Iterator *iterator = directory_iterator(paths->source, paths->source, TRUE);
  while(has_next(iterator)) {
    FileInfo *file = next_file(iterator);

    if(file->hasExtension && strcmp(file->extension, "h") == EQUAL) {
      sprintf(command, "cp %s %s/%s.h", file->fullname, paths->distribution, file->name);
      system(command);
    }
  }

  iterator_destroy(iterator);
  free(command);
}

static void createDistribution(Project *project, ProjectPaths *paths) {
  chdir(paths->distribution);
  char *headers = join(files_by_extension(".", "h", FALSE, TRUE), " ", NULL);

  char *command = (char *)malloc(sizeof(char) * 4096);
  if(project->type == DynamicLibrary) {
    sprintf(command, "tar -cf %s-%s.tar %s-%s.%s %s library.ini",
      project->name,
      project->version,
      project->name,
      project->version,
      EXTENSION_DYNAMIC_LIBRARY,
      headers
    );
  } else {
    sprintf(command, "tar -cf %s-%s.tar %s.%s %s library.ini",
      project->name,
      project->version,
      project->name,
      EXTENSION_STATIC_LIBRARY,
      headers
    );
  }
  system(command);

  sprintf(command, "gzip %s-%s.tar", project->name, project->version);
  system(command);

  directory_new("include");
  system("mv *.h include/");

  free(command);
  free(headers);
  chdir("..");
}

static void extractDependencies(Project *project) {
  Iterator *iterator = directory_iterator(".", "", FALSE);

  while(has_next(iterator)) {
    FileInfo *file = next_file(iterator);

    if(file->hasExtension && strcmp(file->extension, "a") == EQUAL) {
      char *command = (char *)malloc(sizeof(char) * 4096);
      sprintf(command, "ar -x %s.a", file->name);
      system(command);

      free(command);
    }
  }

  iterator_destroy(iterator);
}
