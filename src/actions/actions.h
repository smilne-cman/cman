#ifndef __ACTIONS__
#define __ACTIONS__

/* Includes *******************************************************************/
#import "../service/project.h"

/* Types **********************************************************************/
typedef enum ActionType {
  ActionCreateProject,
  ActionNewFile,
  ActionCleanBuild,
  ActionCompileProject,
  ActionLinkProject,
  ActionCompileTests,
  ActionRunTests,
  ActionPackageProject,
  ActionInstallSymlink,
  ActionPackageLibrary,
  ActionPublishLibrary
} ActionType;

typedef struct Action {
  ActionType type;
  void *data;
} Action;

typedef struct ActionCreateProjectData {
  char *name;
  char *version;
  char *description;
  ProjectType type;
} ActionCreateProjectData;

typedef struct ActionNewFileData {
  char *name;
  char *module;
  int createSource;
  int createHeader;
  int createTest;
} ActionNewFileData;

/* Macros *********************************************************************/

/* Global Functions ***********************************************************/
void invoke(Action *action);

Action *action_create_project(char *name, char *version, char *description, ProjectType type);
void create_project(ActionCreateProjectData *data);

Action *action_new_file(char *name, char *module, int createSource, int createHeader, int createTest);
void new_file(ActionNewFileData *data);

Action *action_clean_build();
void clean_build();

Action *action_compile_project();
void compile_project();

Action *action_link_project();
void link_project();

Action *action_compile_tests();
void compile_tests();

Action *action_run_tests();
void run_tests();

Action *action_package_project();
void package_project();

Action *action_install_symlink();
void install_symlink();

Action *action_package_library();
void package_library();

#endif
