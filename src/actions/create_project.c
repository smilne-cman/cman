#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <libcollection/list.h>
#include "actions.h"
#include "../labels.h"
#include "../templates.h"
#include "../service/repository.h"
#include "../service/project.h"

/* Types **********************************************************************/

/* Prototypes *****************************************************************/
static void create_project_ini(ActionCreateProjectData *data);
static void create_project_dirs();
static void create_project_repo(char *name);
static void create_project_gitignore(int isLibrary, char *name);
static int is_lib(char *name);
static void create_project_source(char *name);

/* Global Variables ***********************************************************/

/* Functions ******************************************************************/
Action *action_create_project(char *name, char *version, char *description, ProjectType type) {
  Action *action = (Action *)malloc(sizeof(Action));

  action->type = ActionCreateProject;

  ActionCreateProjectData *data = (ActionCreateProjectData *)malloc(sizeof(ActionCreateProjectData));

  data->name = name;
  data->version = version;
  data->description = description;
  data->type = type;

  action->data = data;

  return action;
}

void create_project(ActionCreateProjectData *data) {
  ProjectService *service = get_project_service();
  Project *project = service->current();

  if(project != NULL) {
    puts(LABEL_PROJECT_EXISTS_ERROR);
    return;
  }

  printf("Creating new project %s...\n", data->name);

  create_project_ini(data);
  create_project_dirs();
  create_project_source(data->name);
  create_project_repo(data->name);

  // Install libraries
  project = service->current();
  RepositoryService *repository = get_repository_service();
  repository->get("libtest", NULL, project->paths->library);
}

static void create_project_ini(ActionCreateProjectData *data) {
  IniFile *file = ini_open(is_lib(data->name) ? "library.ini": "project.ini");
  ini_put(file, NULL, "name", data->name);

  if(data->type == StandardProject) {
    ini_put(file, NULL, "type", "project");
  } else {
    ini_put(file, NULL, "type", data->type == DynamicLibrary ? "dynamic" : "static");
  }

  ini_put(file, NULL, "version", data->version);
  ini_put(file, NULL, "description", data->description);
  ini_put(file, "library", "libtest", "1.0.0");

  ini_write(file);
  ini_close(file);
}

static void create_project_dirs() {
  system("mkdir doc");
  system("mkdir lib");
  system("mkdir res");
  system("mkdir src");
  system("mkdir test");
  system("mkdir tmp");
}

static void create_project_repo(char *name) {
  create_project_gitignore(is_lib(name), name);

  system("git init");
  system("git add .");
  system("git commit -m \"INITIAL COMMIT\"");
}

static void create_project_gitignore(int isLibrary, char *name) {
  List *paths = list_new(ListString);
  list_adds(paths, ".DS_STORE");
  list_adds(paths, "bin");
  list_adds(paths, "dist");
  list_adds(paths, "lib/**");
  list_adds(paths, "build.log");
  list_adds(paths, "tmp");
  if(!is_lib(name)) list_adds(paths, name);
  template_gitignore(paths);
}

static int is_lib(char *name) {
  if(name == NULL) return FALSE;

  return strstr(name, "lib") != NULL;
}

static void create_project_source(char *name) {
  if(is_lib(name)) {
    invoke(action_new_file(name, NULL, FALSE, TRUE, FALSE));
  } else {
    invoke(action_new_file("main", NULL, TRUE, FALSE, FALSE));
  }
}
