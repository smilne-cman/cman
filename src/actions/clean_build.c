#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <libfiles/libfiles.h>

#include "actions.h"
#include "../labels.h"
#include "../templates.h"

/* Types **********************************************************************/

/* Prototypes *****************************************************************/

/* Global Variables ***********************************************************/

/* Functions ******************************************************************/
Action *action_clean_build() {
  Action *action = (Action *)malloc(sizeof(Action));

  action->type = ActionCleanBuild;

  return action;
}

void clean_build() {
  Project *project = get_project_service()->current();

  ProjectPaths *paths = project->paths;
  directory_remove(paths->binary);
  directory_remove(paths->distribution);
  directory_remove(paths->temporary);
  system("rm build.log");

  directory_new(paths->temporary);
}
