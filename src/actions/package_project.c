#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "actions.h"

#include <libfiles/libfiles.h>
#include "actions.h"
#include "../labels.h"
#include "../utility.h"
#include "../templates.h"
#include "../constants.h"

/* Types **********************************************************************/

/* Prototypes *****************************************************************/

/* Global Variables ***********************************************************/

/* Functions ******************************************************************/
Action *action_package_project() {
  Action *action = (Action *)malloc(sizeof(Action));

  action->type = ActionPackageProject;

  return action;
}

void package_project() {
  Project *project = get_project_service()->current();
  ProjectPaths *paths = project->paths;

  directory_new(paths->distribution);
  system("echo >> build.log \"====> Packaging project...\"");

  char *command = (char *)malloc(sizeof(char) * 1024);
  sprintf(command, "cp %s/* %s 2>> build.log", paths->documentation, paths->distribution);
  system(command);

  sprintf(command, "cp %s/%s %s/%s 2>> build.log", paths->binary, project->name, paths->distribution, project->name);
  system(command);

  sprintf(command, "cp %s/*.%s %s 2>> build.log", paths->binary, EXTENSION_DYNAMIC_LIBRARY, paths->distribution);
  system(command);

  chdir(paths->distribution);
  char *files = join(files_all(".", "", TRUE), " ", NULL);
  sprintf(command, "tar -cf %s-%s.tar %s 2>> ../build.log", project->name, project->version, files);
  system(command);

  sprintf(command, "gzip %s-%s.tar 2>> ../build.log", project->name, project->version);
  system(command);

  sprintf(command, "rm %s 2>> ../build.log", files);
  system(command);

  free(files);
  free(command);

  chdir("..");
}
