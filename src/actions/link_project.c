#include <stdio.h>
#include <stdlib.h>

#include <libfiles/libfiles.h>
#include <libcollection/list.h>
#include <libcollection/iterator.h>
#include "../utility.h"
#include "../labels.h"
#include "../constants.h"
#include "actions.h"

/* Types **********************************************************************/

/* Prototypes *****************************************************************/
static const char *get_dynamic_libraries(const char *path);

/* Global Variables ***********************************************************/

/* Functions ******************************************************************/
Action *action_link_project() {
  Action *action = (Action *)malloc(sizeof(Action));

  action->type = ActionLinkProject;

  return action;
}

void link_project() {
  Project *project = get_project_service()->current();
  ProjectPaths *paths = project->paths;

  if(project->isLibrary) {
    return;
  }

  printf("Linking project...\n");
  directory_new(paths->distribution);
  chdir(paths->binary);

  char *objects = join(files_by_extension(".", "o", TRUE, TRUE), " ", NULL);
  char *libs = join(files_by_extension(file_prefix("..", paths->library, "/"), EXTENSION_STATIC_LIBRARY, TRUE, FALSE), " ", NULL);
  const char *dynamicLibs = get_dynamic_libraries(paths->library);

  const char *command = (const char *)malloc(sizeof(char) * 4096);
  system("echo >> ../build.log \"====> Linking Project...\"");
  sprintf(command, "gcc %s %s -o %s -I../%s -L. %s 2>> ../build.log", objects, libs, project->name, paths->library, dynamicLibs);
  if(system(command)) {
    system("cat ../build.log");
    printf("\n\n%s\n", command);
    printf("\n\nError linking project!\n");
    exit(1);
  }

  free(libs);
  free(objects);
  free(command);

  chdir("..");
}

static const char *get_dynamic_libraries(const char *path) {
  List *files = files_by_extension(file_prefix("..", path, "/"), EXTENSION_DYNAMIC_LIBRARY, TRUE, FALSE);
  char *libs = (char *)malloc(sizeof(char) * 2048);

	libs[0] = NULL;

  Iterator *iterator = list_iterator(files);
  while(has_next(iterator)) {
    FileInfo *file = next_file(iterator);

    sprintf(libs, "%s -l%s", libs, file->name + 3);
  }
  iterator_destroy(iterator);

  return libs;
}
