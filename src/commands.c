#include <stdio.h>
#include <stdlib.h>
#include <libcommand/libcommand.h>

#include "labels.h"
#include "commands.h"
#include "actions/actions.h"
#include "service/project.h"
#include "command/handle_new.h"
#include "command/handle_test.h"
#include "command/handle_init.h"
#include "command/handle_clean.h"
#include "command/handle_compile.h"
#include "command/handle_package.h"
#include "command/handle_symlink.h"
#include "command/handle_publish.h"
#include "command/handle_install.h"
#include "command/handle_uninstall.h"
#include "command/handle_config.h"

/* Types **********************************************************************/

/* Prototypes *****************************************************************/
static void on_unknown(List *arguments);
static void auto_build();

/* Global Variables ***********************************************************/

/* Functions ******************************************************************/
CommandHandler *get_handler() {
  CommandHandler *handler = command_new("", NULL, on_unknown);

  command_add(handler, command_new("init", resolve_init, on_init));
  command_add(handler, command_new("new", resolve_new, on_new));
  command_add(handler, command_new("clean", NULL, on_clean));
  command_add(handler, command_new("compile", NULL, on_compile));
  command_add(handler, command_new("test", NULL, on_test));
  command_add(handler, command_new("package", NULL, on_package));
  command_add(handler, command_new("symlink", NULL, on_symlink));
  command_add(handler, command_new("publish", NULL, on_publish));
  command_add(handler, command_new("install", resolve_install, on_install));
  command_add(handler, command_new("uninstall", NULL, on_uninstall));
  command_add(handler, command_new("config", resolve_config, on_config));

  return handler;
}

static void on_unknown(List *arguments) {
  if (list_size(arguments) == 0) {
    auto_build();
    return;
  }

  puts(LABEL_HELP);
}

static void auto_build() {
  Project *project = get_project_service()->current();
  if(project == NULL) {
    warning(LABEL_PROJECT_NOT_EXISTS_ERROR);
    return;
  }

  puts("Cleaning project...");
  invoke(action_clean_build());

  puts(project->isLibrary ? "Compiling library..." : "Compiling project...");
  invoke(action_compile_project());

  puts("Compiling unit tests...");
  invoke(action_compile_tests());

  puts("Running unit tests...");
  invoke(action_run_tests());

  if(project->isLibrary) {
    puts("Packaging library...");
    invoke(action_package_library());
  } else {
    puts("Packaging project...");
    invoke(action_package_project());
  }
}
