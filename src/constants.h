#ifndef __CONSTANTS__
#define __CONSTANTS__

/* Includes *******************************************************************/

/* Types **********************************************************************/

/* Macros *********************************************************************/

/* Global Variables ***********************************************************/
extern int PROJECT_NAME_MAX_LENGTH;
extern int PROJECT_VERSION_MAX_LENGTH;
extern int PROJECT_DESCRIPTION_MAX_LENGTH;

extern const char EXTENSION_STATIC_LIBRARY[];
extern const char EXTENSION_DYNAMIC_LIBRARY[];

#endif
